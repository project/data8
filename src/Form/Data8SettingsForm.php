<?php

/**
 * @file
 * Contains \Drupal\data8\Form\Data8SettingsForm
 */

 namespace Drupal\data8\Form;

 use Drupal\Core\Form\ConfigFormBase;
 use Drupal\Core\Form\FormStateInterface;

 class Data8SettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'data8_settings_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    //Form constructor
    $form = parent::buildForm($form, $form_state);
    //Default settings
    $config = $this->config('data8.settings');
    $form['apikey_prompt'] = array(
      '#type' => 'item',
      '#markup' =>$this->t("<div style='padding-bottom: 10px;'>Please enter your 
      <a href='https://www.data-8.co.uk/dashboard/api-keys/' target='_blank'>Data8 API key below</a>. 
      <strong>Haven't got a Data8 account yet? <a href='https://www.data-8.co.uk/contact/' target='_blank'>Get a free trial now</a></strong></div>"),
    );
    //Client API key field
    $form['client_api_key'] = array (
      '#type' => 'textfield',
      '#title' => $this->t('Client-Side API Key'),
      '#default_value' => $config->get('data8.client_api_key'),
    );
    // Email Validation Level
    $form['email_validation_level'] = array(
      '#type' => 'select',
      '#title' => $this->t('Email Validation Level'),
      '#options' => [
        'None' => $this->t('None'),
        'Syntax' => $this->t('Syntax'),
        'MX' => $this->t('MX'),
        'Server' => $this->t('Server'),
        'Address' => $this->t('Address')
      ],
      '#default_value' => $config->get('data8.email_validation_level'),
    );
    $form['email_syntax'] = array(
      '#type' => 'item',
      '#markup' =>$this->t('<div> - The supplied email is checked to ensure that it meets the standard email address format.</div>'),
      '#states' => [
        'visible' => [
          ':input[name="email_validation_level"]' => ['value' => 'Syntax'],
        ],
      ],
    );
    $form['email_mx'] = array(
      '#type' => 'item',
      '#markup' =>$this->t('<div> - The supplied email is checked to ensure that the domain name (the part to the right of the @ sign) exists and is set up to receive email.</div>'),
      '#states' => [
        'visible' => [
          ':input[name="email_validation_level"]' => ['value' => 'MX'],
        ],
      ],
    );
    $form['email_server'] = array(
      '#type' => 'item',
      '#markup' =>$this->t('<div> - In addition to the Domain level checks, Server level validation ensures that at least one of the mail servers advertised for the domain is actually live</div>'),
      '#states' => [
        'visible' => [
          ':input[name="email_validation_level"]' => ['value' => 'Server'],
        ],
      ],
    );
    $form['email_address'] = array(
      '#type' => 'item',
      '#markup' =>$this->t('<div> - In addition to the Server level checks, Address level validation ensures that the mail server accepts mail for the full email address.</div>'),
      '#states' => [
        'visible' => [
          ':input[name="email_validation_level"]' => ['value' => 'Address'],
        ],
      ],
    );
    $form['phone_validation'] = array(
      '#type' => 'select',
      '#title' => $this->t('Phone Validation'),
      '#options' => [
        'On' => $this->t('On'),
        'Off' => $this->t('Off')
      ],
      '#default_value' => $config->get('data8.phone_validation')
    );
    $form['phone_help'] = array(
      '#type' => 'item',
      '#markup' =>$this->t('<div style="padding-bottom: 10px;">Enter any advanced options for Phone Validation below, for example:</div>
        <div>TreatUnavailableMobileAsInvalid: true</div>
        <div>ExcludeUnlikelyNumbers: true</div>
        <div style="padding-top: 10px;"><a href="https://www.data-8.co.uk/resources/api-reference/phonevalidation/isvalid/" target="_blank">See the full list of options</a>.</div>'),
      '#states' => [
        'visible' => [
          ':input[name="phone_validation"]' => ['value' => 'On'],
        ],
      ],
    );
    $form['phone_validation_advanced'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Phone Validation Advanced Options'),
      '#default_value' => $config->get('data8.phone_validation_advanced_options'),
      '#states' => [
        'visible' => [
          ':input[name="phone_validation"]' => ['value' => 'On'],
        ],
      ],
    );
    

    $form['predictive_address'] = array(
      '#type' => 'select',
      '#title' => $this->t('Predictive Address'),
      '#options' => [
        'On' => $this->t('On'),
        'Off' => $this->t('Off')
      ],
      '#default_value' => $config->get('data8.predictive_address')
    );
    $form['pa_help'] = array(
      '#type' => 'item',
      '#markup' =>$this->t('<div style="padding-bottom: 10px;">Enter any customisation options for PredictiveAddress below, for example:</div>
        <div>allowedCountries: ["GB", "US"]</div>
        <div>includeNYB: true</div>
        <div>includeMR: true</div>
        <div style="padding-top: 10px;"><a href="https://www.data-8.co.uk/resources/code-samples/predictiveaddress/" target="_blank">See the full list of options</a>.</div>'),
      '#states' => [
        'visible' => [
          ':input[name="predictive_address"]' => ['value' => 'On'],
        ],
      ],
    );
    $form['predictive_address_advanced'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Predictive Address Advanced Options'),
      '#default_value' => $config->get('data8.predictive_address_advanced_options'),
      '#states' => [
        'visible' => [
          ':input[name="predictive_address"]' => ['value' => 'On'],
        ],
      ],
    );
    
    //end of config screen

    return $form;
  }

  /**
   * {@inheritdoc}.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('data8.settings');
    $config->set('data8.client_api_key', $form_state->getValue('client_api_key'));
    $config->set('data8.email_validation_level', $form_state->getValue('email_validation_level'));
    $config->set('data8.phone_validation', $form_state->getValue('phone_validation'));
    $config->set('data8.phone_validation_advanced_options', $form_state->getValue('phone_validation_advanced'));
    $config->set('data8.predictive_address', $form_state->getValue('predictive_address'));
    $config->set('data8.predictive_address_advanced_options', $form_state->getValue('predictive_address_advanced'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}.
   */
  protected function getEditableConfigNames() {
    return [
      'data8.settings',
    ];
  }
 }